#Make Chat Bot Instance
#Developers - Mihir Jha/Akshay Desai
from chatterbot import ChatBot
from chatterbot.response_selection import get_random_response

import logging
from botConfig import myBotName, chatBG, botAvatar, useGoogle, confidenceLevel
logging.basicConfig(level=logging.INFO)

# Creating ChatBot Instance
chatbot = ChatBot(
    myBotName,
    storage_adapter='chatterbot.storage.SQLStorageAdapter',
    logic_adapters=[
        {
            'import_path': 'chatterbot.logic.BestMatch',
            'default_response': 'IDKresponse',
            'maximum_similarity_threshold': confidenceLevel,
	    'statement_comparison_function': 'comparisons.jaccard_similarity',
	    'response_selection_method': get_random_response
        },
	'chatterbot.logic.MathematicalEvaluation'
    ],
        preprocessors=[
        'chatterbot.preprocessors.clean_whitespace',
        'chatterbot.preprocessors.convert_to_ascii',
        'chatterbot.preprocessors.unescape_html'
    ],
    database_uri='sqlite:///botData.sqlite3'  
) 
