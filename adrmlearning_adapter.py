#Logic Adapter Exit Point Sample
#Demo of how we can implement sample custom logic
#Developers - Mihir Jha/Akshay Desai
from chatterbot.logic import LogicAdapter
from chatterbot.conversation import Statement
import requests

class MyLogicAdapter(LogicAdapter):

    def __init__(self, chatbot, **kwargs):
        super().__init__(chatbot, **kwargs)

    def can_process(self, statement):
        """
        Return true if the input statement contains
        'adrm' and 'ep' and 'learning'.
        """
        words = ['reveneuone','REVENUEONE','R1', 'r1','adrm', 'ADRM', 'RM', 'rm','ep', 'EP','rlc','RLC','TC','tc' , 'jam', 'JAM', 'page' ,'training', 'TRAIN','CM','cm','CEO','ceo','AR','ar','ngrlc','NGRLC','collection','COLLECTION','INVOICING','invoicing','inv','INV','billing','BILLING','rtb','RTB','cpg','CPG','um','UM','pbm','PBM','tcno','TCNO','TC No Oracle','tc no oracle','TC NO ORACLE','aric','ARIC','spring','SPRING','java','JAVA','microservice','MICROSERVICE','ms360','MS360','postgres','POSTGRES','liquibase','LIQUIBASE','dbeaver','DBEAVER','hbase','HBASE','kafka','KAFKA','couchbase','COUCHBASE','docker','DOCKER','kubernetes','KUBERNETES','k8s','K8S','openshift','OPENSHIFT','helm','HELM','ANSIBLE','ansible','efk','EFK','prometheus','PROMETHEUS','grafana','GRAFANA','airflow','AIRFLOW','postman','POSTMAN']
        #statement = statement.text.lower()        
        if any(x in statement.text.split() for x in words):
            return True
        else:
            return True

    def process(self, input_statement, additional_response_selection_parameters):
        import random

        words = [  'reveneuone','REVENUEONE','R1','r1', 'cpg', 'um','rlc','rating','RM', 'rm','ep', 'EP','rlc','RLC','TC','tc','jam','JAM','CM','cm','CEO','ceo','AR','ar','ngrlc','NGRLC','collection','COLLECTION','INVOICING','invoicing','inv','INV','billing','BILLING','rtb','RTB','cpg','CPG','um','UM','pbm','PBM','tcno','TCNO','TC No Oracle','tc no oracle','TC NO ORACLE','aric','ARIC','spring','SPRING','java','JAVA','microservice','MICROSERVICE','ms360','MS360','postgres','POSTGRES','liquibase','LIQUIBASE','dbeaver','DBEAVER','hbase','HBASE','kafka','KAFKA','couchbase','COUCHBASE','docker','DOCKER','kubernetes','KUBERNETES','k8s','K8S','openshift','OPENSHIFT','helm','HELM','ANSIBLE','ansible','efk','EFK','prometheus','PROMETHEUS','grafana','GRAFANA','airflow','AIRFLOW','postman','POSTMAN']
      
        input_text = input_statement.text.lower()
        
        if any(x in input_text.split() for x in words):
            confidence = 1
        else :
            confidence = 0
        playlist_dict = dict( [('RLC', '<a target="_blank" href="https://doxtube.corp.amdocs.com/playlist/dedicated/1146/0_qmytr52r/0_nro97ho9" > Please Click On This Link</a>'),
                               ('TC', '<a target="_blank" href="https://doxtube.corp.amdocs.com/playlist/dedicated/1146/0_xu37e5xp/0_cebo347n" > Please Click On This Link</a>' ),
                               ('CM','<a target="_blank" href="https://doxtube.corp.amdocs.com/playlist/dedicated/1146/0_c4cc1dq4/0_p0ya4ucp" > Please Click On This Link</a>'),
                               ('NGRLC','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('COLLECTION','<a target="_blank" href="https://doxtube.corp.amdocs.com/playlist/dedicated/1146/0_c4cc1dq4/0_p0ya4ucp" > Please Click On This Link</a>'),
                               ('INVOICING','<a target="_blank" href="https://doxtube.corp.amdocs.com/playlist/dedicated/1146/0_iv9mtz7b/0_r3utqt1a" > Please Click On This Link</a>'),
                               ('INV','<a target="_blank" href="https://doxtube.corp.amdocs.com/playlist/dedicated/1146/0_iv9mtz7b/0_r3utqt1a" > Please Click On This Link</a>'),
                               ('BILLING','<a target="_blank" href="https://doxtube.corp.amdocs.com/playlist/dedicated/1146/0_iv9mtz7b/0_r3utqt1a" > Please Click On This Link</a>'),
                               ('RTB','<a target="_blank" href="https://jam4.sapjam.com/groups/bHzwmROGivIutO9EQzZh7i/overview_page/ROFi0ONh5TaC89xcuY0BX4" > Please Click On This Link</a>'),
                               ('CPG','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('UM','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('PBM','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('TCNO','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('TC NO ORACLE','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('TC No Oracle','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('R1','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/hWTik1FuwTL3VznFUw0c1K" > Please Click On This Link</a>'),
                               ('REVENUEONE','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/hWTik1FuwTL3VznFUw0c1K" > Please Click On This Link</a>'),
                               ('EP','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('ARIC','<a target="_blank" href="https://jam4.sapjam.com/groups/bHzwmROGivIutO9EQzZh7i/overview_page/ROFi0ONh5TaC89xcuY0BX4" > Please Click On This Link</a>'),
                               ('JAM','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/hWTik1FuwTL3VznFUw0c1K" > Please Click On This Link</a>'),
                               ('SPRING','<a target="_blank" href="https://jam4.sapjam.com/groups/OhaxGkMfExsUNyHy5PNvSS/overview_page/Zsxpa7bdcCt5AeblEDyx3x" > Please Click On This Link</a>'),
                               ('JAVA','<a target="_blank" href="https://jam4.sapjam.com/groups/v49OATN3PdrLZZclo2uyIV/overview_page/qgTHiK018oxF5WYB7iGXoo" > Please Click On This Link</a>'),
                               ('MICROSERVICE','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/KQCsufrMTlf871D6U0SFBv" > Please Click On This Link</a>'),
                               ('MS360','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/KQCsufrMTlf871D6U0SFBv" > Please Click On This Link</a>'),
                               ('POSTGRES','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('DBEAVER','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('LIQUIBASE','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('HBASE','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('KAFKA','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('COUCHBASE','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('DOCKER','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('KUBERNETES','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('OPENSHIFT','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('HELM','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('ANSIBLE','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('EFK','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('PROMETHEUS','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('GRAFANA','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('AIRFLOW','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('POSTMAN','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('K8S','<a target="_blank" href="https://jam4.sapjam.com/groups/w2S8fyWpZrfbQEDK0u3Adu/overview_page/eCbaH6M0SQvefRK5FbqqtH" > Please Click On This Link</a>'),
                               ('AR', '<a target="_blank" href="https://doxtube.corp.amdocs.com/playlist/dedicated/1146/0_c4cc1dq4/0_p0ya4ucp" > Please Click On This Link</a>' )])
       
        txt = '<a class="item_link" href="https://doxtube.corp.amdocs.com/media/RLC_Session_Segments_And_Extension_Functions/0_gktsokdn/1146" >RLC_Session_Segments_And_Extension_Functions</a>'
        txt2 = '<a class="item_link" href="https://doxtube.corp.amdocs.com/playlist/dedicated/1146/0_qmytr52r/0_nro97ho9" target="_blank" >RLC Learning Video Playlist</a>'
        
        response_text='Please use following learning page: \n' + txt + '\n' + txt2
        
        if input_text.find(' rlc') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['RLC']
        elif input_text.find('cm') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['CM']
        elif input_text.find('ngrlc') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['NGRLC']
        elif input_text.find('collection') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['COLLECTION']
        elif input_text.find('invoicing') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['INVOICING']
        elif input_text.find('inv') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['INV']
        elif input_text.find('billing') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['BILLING']
        elif input_text.find('rtb') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['RTB']
        elif input_text.find('cpg') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['CPG']
        elif input_text.find('um') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['UM']
        elif input_text.find('pbm') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['PBM']
        elif input_text.find('tc no oracle') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['TC NO ORACLE']
        elif input_text.find('tcno') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['TCNO']
        elif input_text.find('r1') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['R1']
        elif input_text.find('revenueone') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['REVENUEONE']
        elif input_text.find('ep') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['EP']
        elif input_text.find('aric') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['ARIC']
        elif input_text.find('jam') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['JAM']
        elif input_text.find('spring') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['SPRING']
        elif input_text.find('java') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['JAVA']
        elif input_text.find('microservice') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['MICROSERVICE']
        elif input_text.find('ms360') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['MS360']
        elif input_text.find('k8s') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['K8S']
        elif input_text.find('postgres') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['POSTGRES']
        elif input_text.find('dbeaver') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['DBEAVER']
        elif input_text.find('liquibase') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['LIQUIBASE']
        elif input_text.find('hbase') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['HBASE']
        elif input_text.find('kafka') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['KAFKA']
        elif input_text.find('couchbase') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['COUCHBASE']
        elif input_text.find('docker') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['DOCKER']
        elif input_text.find('kubernetes') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['KUBERNETES']
        elif input_text.find('openshift') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['OPENSHIFT']
        elif input_text.find('helm') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['HELM']
        elif input_text.find('ansible') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['ANSIBLE']
        elif input_text.find('efk') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['EFK']
        elif input_text.find('prometheus') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['PROMETHEUS']
        elif input_text.find('grafana') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['GRAFANA']
        elif input_text.find('airflow') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['AIRFLOW']
        elif input_text.find('postman') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['POSTMAN']
        elif input_text.find(' tc') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['TC']
        elif input_text.find(' ar') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['AR']
        elif input_text.find('rlc') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['RLC']
        elif input_text.find('tc') >  -1 :
            response_text='Please use following learning page: \n' + playlist_dict['TC']	
        else :
            response_text='No such course found. Please check your input/spelling. Try to put all characters in lower case \n'
        
        selected_statement = Statement(text=response_text)

        selected_statement.confidence = confidence

        return selected_statement
        
    