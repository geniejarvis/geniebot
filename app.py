#Host Flask App for our chatbot
#Developers - Akshay Desai/Mihir Jha
from chatbot import chatbot
from chatterbot.conversation import Statement
from chatterbot.trainers import ListTrainer
from flask import Flask, render_template, request
import logging
import random
import csv
import os
from botConfig import myBotName,botTimeZone,botAvatar,personAvatar,chatBG,useGoogle,confidenceLevel
import pyttsx3
import speech_recognition as sr

##Experimental Date Time
from dateTime import getTime, getDate

logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
app.static_folder = 'static'

def tryGoogle(myQuery):
	#print("<br>Try this from my friend Confluence: <a target='_blank' href='" + j + "'>" + query + "</a>")
	return "<br><br>You can try this from my friend Confluence: <a target='_blank' href='https://confluence/dosearchsite.action?queryString=" + myQuery + "'>" + myQuery + "</a>"

@app.route("/")
def home():
    return render_template("index.html",botName = myBotName, chatBG = chatBG, botAvatar = botAvatar, personAvatar =personAvatar)

@app.route("/get")
def get_bot_response():
    userText = request.args.get('msg')
    #print(userText)
    #b = TextBlob(userText)
    #print("corrected text: "+str(b.correct()))
    #self.logger.info('HHHHHHHH {}'.format(userText))
    print("HHHHHHH: "+userText)
    if userText.find("TRAINBOT") > -1 :
        question_text = userText[userText.find('#Q#')+3:userText.find('#/Q#')]
        answer_text = userText[userText.find('#A#')+3:userText.find('#/A#')]
        input_statement = Statement(text=question_text)
        correct_response = Statement(text=answer_text)
 
 # Training with Personal Ques & Ans
        conv = [
            question_text,
            answer_text
        ]

        trainr = ListTrainer(chatbot)
        trainr.train(conv)
        
        
        #chatbot.learn_response(correct_response, input_statement)
        return str('Thanks for teaching me. I learnt !!')
    else :
        botReply = str(chatbot.get_response(userText.lower()))
        if botReply =="IDKresponse":
            botReply = str(chatbot.get_response('IDKnull')) ##Send the i don't know code back to the DB
            if useGoogle == "yes":
                botReply = botReply + tryGoogle(userText)
        elif botReply == "getTIME":
            botReply = getTime()
            print(getTime())
        elif botReply == "getDATE":
            botReply = getDate()
            print(getDate())

        ##Log to CSV file if confidenceLevel is less
       
        print("Logging to CSV file now")
        with open('BotLog.csv', 'a', newline='') as logFile:
                newFileWriter = csv.writer(logFile)
                newFileWriter.writerow([userText, botReply])
                logFile.close()

        engine = pyttsx3.init('sapi5')
        voices = engine.getProperty('voices')
        engine.setProperty('voice', voices[0].id)  # voices[0] for men voice and 1 for women voice
        engine.setProperty("rate", 200)  # to increase/decrease voice rate
        if "<br><br>" in botReply:
            engine.setProperty('voice', voices[0].id)
            engine.say(botReply.split("<br>", 2)[0])
        elif "Heres the link" in botReply:
            engine.setProperty('voice', voices[1].id)
            engine.say("Heres the link")
        elif "Here is the link" in botReply:
            engine.setProperty('voice', voices[1].id)
            engine.say("Here is the link for more details")
        else:
            engine.say(botReply)
        engine.runAndWait()
        return botReply


        return str(chatbot.get_response(userText))

@app.route('/mic')
def speechToText():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print('Say what u want me to do')
        audio = r.listen(source)
        print("Ok!! Got it")
    p = r.recognize_google(audio)
    print(p)

    return (p)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8086)