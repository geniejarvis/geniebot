#! /usr/bin/python3

#from flask import Flask, render_template, request

from chatterbot.trainers import ChatterBotCorpusTrainer

#For getting the spreadsheet data from csv
import os
import csv

import logging
logging.basicConfig(level=logging.INFO)

if os.path.exists("botData.sqlite3"):
    os.remove("botData.sqlite3")
    print("Clearing my old training data.")

from chatbot import chatbot
  
lineCount = 0
successCount = 0
emptyCount = 0
with open('data/trainingdata.yml', 'w') as f:
    f.write("categories:\n")
    f.write("- profile")
    f.write("\nconversations:")
    with open('data/chatbot.csv') as g:
        lines = csv.reader(g)
        for line in lines:
            lineCount += 1
            if not line[0] or not line[1]:
                emptyCount += 1
                print("WARNING: I had to skip row #" + str(lineCount) + " due to missing data.")
            if lineCount > 1 and line[0] and line[1]:
                successCount += 1
                f.write("\n- - " + line[0])
                f.write("\n  - " + line[1])

print("==============================================")
print("There are " + str(lineCount - 1) + " rows in chatbot.csv")
print("==============================================")
print("There were " + str(emptyCount) + " empty cells that I could not use for training.")
print("==============================================")
print("I have successfully imported " + str(successCount) + " rows of info and will now retrain...")
print("==============================================")



# Training with English Corpus Data 
trainer_corpus = ChatterBotCorpusTrainer(chatbot)
trainer_corpus.train(
	'data/trainingdata.yml',
	'data/english/botprofile.yml',
	'chatterbot.corpus.english',
	'chatterbot.corpus.hindi'
)


print("I am all trained up and ready to chat!")