#! /usr/bin/python3

##Change the following values to customize your ChatBot:
myBotName = "Genie"
botTimeZone = "Asia/Kolkata"  ##See the list full below
botAvatar = "/static/genie.png"
personAvatar = "/static/person.png"
chatBG = "black.jpg"  ##This can either be an image in your static folder or a web url to an image
useGoogle = "yes" ## yes or no if you want Google links provided when your Bot does not have an answer
confidenceLevel = 0.70 ##Bot confidence level - must be between 0.00 and 1, default is 0.55